﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PipeFilter
{
    public interface IFilter
    {
        GPA doFilter(GPA input);
    }
}
