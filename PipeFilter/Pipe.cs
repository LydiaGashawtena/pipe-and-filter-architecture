﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PipeFilter
{
    public class Pipe
    {
        List<IFilter> filters = new List<IFilter>();
        public Pipe()
        {
            filters.Add(new MultiplyFilter());
            filters.Add(new AddFilter());
            filters.Add(new DivideFilter());
            
        }

        public GPA calculate(GPA input)
        {
            foreach (var filter in filters)
            {
                input = filter.doFilter(input);
            }

            return input;
        }
        
    }
}
