﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PipeFilter
{
    public class GPA
    {
        public List<Course> courses { get; set; }
        public List<double> ectsXgrade { get; set; }
        public double sum { get; set; }
        public int ectsSum { get; set; }
        public double gpa { get; set; }
    }
}
