﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PipeFilter
{
    public class MultiplyFilter: IFilter
    {
        public GPA doFilter(GPA input)
        {
            List<double> temp = new List<double>();

            foreach (var course in input.courses)
            {
                temp.Add(course.ects * course.grade);
            }
            input.ectsXgrade = temp;

            return input;
        }

    }
}
