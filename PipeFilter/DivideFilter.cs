﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PipeFilter
{
    public class DivideFilter : IFilter
    {
        public GPA doFilter(GPA input)
        {
            input.gpa = input.sum / input.ectsSum;

            return input;
        }
    }
}
