﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PipeFilter
{
    public class AddFilter : IFilter
    {
        public GPA doFilter(GPA input)
        {
            double sum = 0;
            int ectsSum = 0;

            foreach (var multiple in input.ectsXgrade)
            {
                sum += multiple;
            }

            foreach (var course in input.courses)
            {
                ectsSum += course.ects;
            }

            input.sum = sum;
            input.ectsSum = ectsSum;

            return input;
        }
    }
}
